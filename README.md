# MERN APP

This is a demo app for showing login, registration and list of registered users.

## Technical Stack

  - FrontEnd:- ReactJs(Redux)
  - Backend:- Expressjs(NodeJS)
  - Database:- MongoDB

## Source Code Link
    
- FrontEnd:- https://gitlab.com/prashantk2/mern-login-front
- BackEnd:- https://github.com/prashantk2/mern-login-backend

## APP Hosting
The app(both frontEnd and backEnd) is hosted on Netlify. Database is hosted on MongoDB Atlas cloud.

- FrontEnd Link:- https://mern-login-demo.netlify.app
- BackEnd Link:- https://mern-login-backend.herokuapp.com

## APP Summary
##### List of pages of the App
- Login
- Registration
- Dashboard
- User Details
- User Edit

##### Extra Adds
I have added eslint, husky and lint-staged dependency to check linting and to handle git hooks so that before commit it checks all the added file and show error.

##### Working
Once the user sucessfully login then he is redirected to the Dashboard where list of all the available user is shown. One can edit the details of the user and can also delete them.