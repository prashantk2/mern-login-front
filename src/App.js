import React from 'react';
import './App.css';
import Login from './components/Login';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Register from './components/Register';
import Dashboard from './components/Dashboard';
import PrivateRoute from './components/PrivateRoute';
import store from './configureStore';
import jwt_decode from 'jwt-decode';
import { setLoginSuccess } from './store/actions/AuthActions';
import UserDetailsComponent from './components/UserDetailsComponent';
import Navbar from './components/Navbar';
import NotFoundComponent from './components/NotFoundComponent';
import setAuthToken from './setAuthToken';
import EditUser from './components/EditUser';

// Check for token to keep user logged in
if (localStorage.customerToken) {
  const token = localStorage.customerToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setLoginSuccess(decoded));
  // const currentTime = Date.now() / 1000; // to get in milliseconds
  // if (decoded.exp < currentTime) {
  //   store.dispatch(logoutUser());
  //   window.location.href = "./login";
  // }
}

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        
        <Switch>
          <Route
            component={Login}
            exact
            path="/login"
          />
          <Route
            component={Register}
            exact
            path="/register"
          />
          <PrivateRoute
            component={Dashboard}
            exact
            path="/"
          />
          <PrivateRoute
            component={UserDetailsComponent}
            exact
            path="/user-details"
          />
          <PrivateRoute
            component={EditUser}
            exact
            path="/edit-details/:id"
          />
          <Route component={NotFoundComponent} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
