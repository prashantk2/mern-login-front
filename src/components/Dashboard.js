import React, { Component } from 'react';
import {connect} from 'react-redux';
import { listOfAllUsers } from '../store/actions/UserActions';
import './Dashboard.css';
import { SUCCESS } from '../store/types';
import Loader from './Loader';
import PropTypes, { array } from 'prop-types';
import { Link } from 'react-router-dom'

class Dashboard extends Component {
  componentDidMount() {
    this.props.listOfAllUsers();
  }
  render() {
    return (
      <React.Fragment>
        <div>
                These are the list of all the members.
        </div>
        {
          this.props.isListOfAllUsersStatus === SUCCESS && 
                this.props.listOfAllUsersData && 
                this.props.listOfAllUsersData.data ? (
              <div style={{marginTop: '5%'}}>
                <table className="main-table">
                  <thead>
                    <tr>
                      <td>S No.</td>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Role</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.listOfAllUsersData.data.length > 1 &&
                    this.props.listOfAllUsersData.data.map((data, i) => (
                      <React.Fragment key={data._id}>
                        <tr>
                          <td>{++i}</td>
                          <td>{data.name}</td>
                          <td>{data.email}</td>
                          <td>{data.role}</td>
                          <td>
                            <Link
                              to={{
                                pathname:`/edit-details/${data._id}`,
                                userDetails:{data: data}
                              }}
                            >
                              Edit
                            </Link>
                          </td>
                        </tr>
                      </React.Fragment>
                    ))
                    }
                  </tbody>
                </table>
              </div>
            ) : <Loader />
        }
                
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isListOfAllUsersStatus: state.userReducer.isListOfAllUsersStatus,
    listOfAllUsersData: state.userReducer.listOfAllUsers,
    listOfAllUsersError: state.userReducer.listOfAllUsersError
  };
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    listOfAllUsers: () => dispatch(listOfAllUsers()),
  };
};
  
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

Dashboard.propTypes = {
  isListOfAllUsersStatus: PropTypes.string,
  listOfAllUsers: PropTypes.func,
  listOfAllUsersData: PropTypes.shape({
    data: array
  })
}