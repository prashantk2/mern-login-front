import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import { getSingleUser, updateUserDetail } from '../store/actions/UserActions';

class EditUser extends Component {
  constructor(props) {
    super(props);
    const userDetails = this.props.location && this.props.location.userDetails ? this.props.location.userDetails : ''
    this.state = {
      name: userDetails ? userDetails.data.name : '',
      email: userDetails ? userDetails.data.email : '',
      errorText: ''
    }
  }
  componentDidMount() {
    const userId = this.props.match.params.id;
    if(this.props.getSingleUser && this.state.name === '') {
      this.props.getSingleUser(userId)
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if(
      this.state.name === '' &&
      nextProps.userDetails &&
      nextProps.userDetails.name && 
      nextProps.userDetails !== ''
    ) {
      this.setState({
        name: nextProps.userDetails ? nextProps.userDetails.name : '',
        email: nextProps.userDetails ? nextProps.userDetails.email : ''
      })
    } else if(this.props.userDataError !== nextProps.userDataError) {
      this.setState({
        errorText: nextProps.userDataError
      })
    } else if(this.props.updateUserStatus !== nextProps.updateUserStatus) {
      this.setState({
        errorText: 'Data Updated Successfully'
      })
    } 
  }

  onSubmit = e => {
    e.preventDefault();
    const userId = this.props.match.params.id;
    const userData = {
      name: this.state.name,
      email: this.state.email,
    };
    this.props.updateUserDetail(userData, userId);
  };

  render() {
    return (
      <div className="main-login-div">
        <form onSubmit={this.onSubmit}>
          <div className="container">
            <h1>Update Details</h1>
            <hr />
            <label htmlFor="name"><b>Name</b></label>
            <input
              name="name"
              onChange={(ele) => this.setState({ name: ele.target.value, errorText: '' })}
              placeholder="Enter Name"
              required
              type="text"
              value={this.state.name}
            />
            <label htmlFor="email"><b>Email</b></label>
            <input
              name="email"
              onChange={(ele) => this.setState({ email: ele.target.value, errorText: '' })}
              placeholder="Enter Email"
              required
              type="text"
              value={this.state.email}
            />
            <hr />
            <input
              className="registerbtn"
              name="Update"
              type="submit"
            />
          </div>
        </form>
        <div>{this.state.errorText}</div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  singleUserStatus: state.userReducer.singleUserStatus,
  userDetails: state.userReducer.singleUser,
  userDataError: state.userReducer.singleUserError,
  updateUserStatus: state.userReducer.updateUserStatus
});
const mapDispatchToProps = (dispatch) => {
  return {
    getSingleUser: (userId) => dispatch(getSingleUser(userId)),
    updateUserDetail: (userData, userId) => dispatch(updateUserDetail(userData, userId))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(EditUser));

EditUser.propTypes = {
  getSingleUser: PropTypes.func,
  location: PropTypes.object,
  match:  PropTypes.object,
  updateUserDetail: PropTypes.func,
  updateUserStatus: PropTypes.string,
  userDataError: PropTypes.string,
  userDetails: PropTypes.object
}
