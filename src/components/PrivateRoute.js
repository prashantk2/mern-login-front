import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PrivateRoute = ({ component: Component, isLoginSuccess, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (
        isLoginSuccess
          ? <Component {...props} />
          : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      )}
    />
  )
}

PrivateRoute.propTypes = {
  isLoginSuccess: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess
});

export default connect(mapStateToProps)(PrivateRoute);

PrivateRoute.propTypes = {
  component: PropTypes.element,
  location: PropTypes.object
}