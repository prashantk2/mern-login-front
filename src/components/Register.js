import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../store/actions/AuthActions';
import './Register.css';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: '',
      password2: '',
      errors: {}
    };
  }
  componentDidMount() {
    if (this.props.isLoginSuccess) {
      this.props.history.push('/');
    }
  }
    onChange = e => {
      this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
      e.preventDefault();
      const newUser = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password2: this.state.password2
      };
      this.props.registerUser(newUser, this.props.history);
    };
    render() {
      let errorText = this.props.regError && this.props.regError.length ?
        this.props.regError.map((err, index) => <span key={index}>{err}.</span>) :
        this.props.regError; 
        
      return (
        <div className="main-login-div">
          <form onSubmit={this.onSubmit}>
            <div className="container">
              <h1>Register</h1>
              <p>Please fill in this form to create an account.</p>
              <hr />
              <label htmlFor="name"><b>Name</b></label>
              <input
                name="name"
                onChange={(ele) => this.setState({ name: ele.target.value })}
                placeholder="Enter Name"
                required
                type="text"
                value={this.state.name}
              />
              <label htmlFor="email"><b>Email</b></label>
              <input
                name="email"
                onChange={(ele) => this.setState({ email: ele.target.value })}
                placeholder="Enter Email"
                required
                type="text"
                value={this.state.email}
              />
              <label htmlFor="psw"><b>Password</b></label>
              <input
                name="password"
                onChange={(ele) => this.setState({ password: ele.target.value })}
                placeholder="Enter Password"
                required
                type="password"
                value={this.state.password}
              />
              <label htmlFor="psw-repeat"><b>Repeat Password</b></label>
              <input
                name="password2"
                onChange={(ele) => this.setState({ password2: ele.target.value })}
                placeholder="Repeat Password"
                required
                type="password"
                value={this.state.password2}
              />
              <hr />
              <button
                className="registerbtn"
                type="submit"
              >Register</button>
            </div>
          </form>
          <div>{errorText}</div>
        </div>
      )
    }
}

const mapStateToProps = state => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess,
  regError: state.AuthReducer.regError
});
export default connect(
  mapStateToProps,
  { registerUser }
)(withRouter(Register));

Register.propTypes = {
  history: PropTypes.object,
  isLoginSuccess: PropTypes.bool.isRequired,
  regError: PropTypes.array,
  registerUser: PropTypes.func
}