import React, { Component } from 'react'
import { connect } from 'react-redux';
import { userDetails } from '../store/actions/AuthActions';
import PropTypes from 'prop-types';
import './UserDetails.css'

class UserDetailsComponent extends Component {
  render() {
    return (
      <div className="main-div">
        This is user details component.
        { this.props.userDetail && this.props.userDetail.data ? (
          <table>
            <tbody>
              <tr>
                <td>Name</td>
                <td>{this.props.userDetail ? this.props.userDetail.data.name : ''}</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>{this.props.userDetail ? this.props.userDetail.data.email : ''}</td>
              </tr>
              <tr>
                <td>Role</td>
                <td>{this.props.userDetail ? this.props.userDetail.data.role : ''}</td>
              </tr>
              <tr>
                <td>Created At</td>
                <td>{this.props.userDetail ? this.props.userDetail.data.created_at : ''}</td>
              </tr>
            </tbody>
          </table>
        ) : ''
        }
        <button onClick={() => this.props.userDetails()}>Click me</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userDetail: state.AuthReducer.userDetail
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    userDetails: () => dispatch(userDetails())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailsComponent);

UserDetailsComponent.propTypes = {
  userDetail: PropTypes.object,
  userDetails: PropTypes.func
}