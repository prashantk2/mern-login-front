import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import AuthReducer from '../src/store/reducers/AuthReducers';
import userReducer from '../src/store/reducers/UserReducer';

const rootReducer = combineReducers({
  AuthReducer,
  userReducer
});

const initialState = {};
const middleware = [thunk];

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware)
  )
);


export default store