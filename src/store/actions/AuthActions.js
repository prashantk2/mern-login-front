import jwt_decode from 'jwt-decode';
import setAuthToken from '../../setAuthToken';
import axios from 'axios';
import { 
  SET_LOGIN_PENDING, 
  SET_LOGIN_SUCCESS, 
  SET_LOGIN_ERROR, 
  GET_ERRORS, 
  SET_USER_DETAILS_PENDING, 
  SET_USER_DETAILS_SUCCESS, 
  SET_USER_DETAILS_ERROR, 
  BASE_URL
} from '../types';
 




const callBackApi = async (email, password, callback) => {
  try {
    const { data } = await axios.post(`${BASE_URL}/api/users/login`, { email, password })
    return callback(data);
  } catch (error) {
    return callback({ success: false, error: 'Invalid Request' });
  }

};

const login = (email, password) => {
  return (dispatch) => {
    try {
      dispatch(setLoginPending(true));
      dispatch(setLoginSuccess(false));
      dispatch(setLoginError(null));

      callBackApi(email, password, (error) => {
        dispatch(setLoginPending(false));
        if (error.success) {
          const { token } = error;
          localStorage.setItem('customerToken', token);
          setAuthToken(token);
          const decoded = jwt_decode(token);
          dispatch(setLoginSuccess(decoded));
        } else {
          dispatch(setLoginError(error));
        }
      });
    } catch (error) {
      dispatch(setLoginError('Error occured'));
    }
  };
};

export function setLoginPending(isLoginPending) {
  return {
    type: SET_LOGIN_PENDING,
    isLoginPending,
  };
}
export function setLoginSuccess(userDetails) {
  return {
    type: SET_LOGIN_SUCCESS,
    userDetails,
  };
}
function setLoginError(loginError) {
  return {
    type: SET_LOGIN_ERROR,
    loginError,
  };
}

function setRegistrationError(regError) {
  return {
    type: GET_ERRORS,
    regError,
  };
}

export const logoutUser = () => dispatch => {
  localStorage.removeItem('customerToken');
  setAuthToken(false);
  dispatch(setLoginSuccess({}));
};

// Register User
export const registerUser = (userData, history) => dispatch => {
  fetch(`${BASE_URL}/api/users/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(userData)
  })
    .then(res => res.json())
    .then(res => {
      if (res.success) {
        history.push('/')
      } else {
        dispatch(setRegistrationError(res.errors))
      }

    })
    .catch(err => {
      dispatch(setRegistrationError([`Error in registering user. ${err} `]))
    }

    );
};

export function setUserDetailsPending(isUserDetailsPending) {
  return {
    type: SET_USER_DETAILS_PENDING,
    isUserDetailsPending,
  };
}
export function setUserDetailsSuccess(data) {
  return {
    type: SET_USER_DETAILS_SUCCESS,
    data
  };
}
function setUserDetailsError(userDetailsError) {
  return {
    type: SET_USER_DETAILS_ERROR,
    userDetailsError,
  };
}

export const userDetails = () => {
  return async (dispatch) => {
    dispatch(setUserDetailsPending(true));
    try {
      const { data } = await axios.get(`${BASE_URL}/api/users/userDetails`);
      if (data.success) {
        dispatch(setUserDetailsSuccess(data));
      } else {
        dispatch(setUserDetailsError(data.error));  
      }
    } catch (error) {
      dispatch(setUserDetailsError('Error in handling request.'));
    }
  }
}

export default login;