import axios from 'axios';
import { 
  BASE_URL, 
  SET_USERS_LIST_PENDING, 
  SET_USERS_LIST_SUCCESS, 
  SET_USERS_LIST_ERROR, 
  REQUESTING,
  SUCCESS,
  FAILURE,
  SET_SINGLE_USER_LIST_PENDING,
  SET_SINGLE_USER_LIST_SUCCESS,
  SET_SINGLE_USER_LIST_ERROR,
  SET_USER_DATA_ERROR,
  SET_USER_DATA_SUCCESS,
  SET_USER_DATA_PENDING
} from '../types';

export function setListOfAllUsersRequesting() {
  return {
    type: SET_USERS_LIST_PENDING,
    status: REQUESTING
  };
}
export function setListOfAllUsersSuccess(usersList) {
  return {
    type: SET_USERS_LIST_SUCCESS,
    status: SUCCESS,
    usersList,
  };
}
function setListOfAllUsersError(usersListError) {
  return {
    type: SET_USERS_LIST_ERROR,
    status: FAILURE,
    usersListError,
  };
}

export const listOfAllUsers = () => {
  return async (dispatch) => {
    dispatch(setListOfAllUsersRequesting());
    try{
      const { data } = await axios.get(`${BASE_URL}/api/users/listOfAllUsers`);
      if (data.success) {
        dispatch(setListOfAllUsersSuccess(data));
      } else {
        dispatch(setListOfAllUsersError(data.error));  
      }
    } catch(err) {
      dispatch(setListOfAllUsersError('Error in handling request.'))
    }
  }
}

export function setGetSingleUserRequesting() {
  return {
    type: SET_SINGLE_USER_LIST_PENDING,
    status: REQUESTING
  };
}
export function setGetSingleUserSuccess(userDetails) {
  return {
    type: SET_SINGLE_USER_LIST_SUCCESS,
    status: SUCCESS,
    userDetails,
  };
}
function setGetSingleUserError(error) {
  return {
    type: SET_SINGLE_USER_LIST_ERROR,
    status: FAILURE,
    error,
  };
}

export const getSingleUser = (userId) => {
  return async (dispatch) => {
    dispatch(setGetSingleUserRequesting());
    try{
      const { data } = await axios.get(`${BASE_URL}/api/users/getSingleUser/${userId}`);
      if (data.success) {
        dispatch(setGetSingleUserSuccess(data));
      } else {
        dispatch(setGetSingleUserError(data.error));  
      }
    } catch(err) {
      if(err.response && err.response.data && err.response.data.error) {
        dispatch(setGetSingleUserError(err.response.data.error))
      } else {
        dispatch(setGetSingleUserError('Error in handling request.'))
      }
      
    }
  }
}


export function setUpdateUserDetailRequesting() {
  return {
    type: SET_USER_DATA_PENDING,
    status: REQUESTING
  };
}
export function setUpdateUserDetailSuccess(userDetails) {
  return {
    type: SET_USER_DATA_SUCCESS,
    status: SUCCESS,
    userDetails,
  };
}
function setUpdateUserDetailError(error) {
  return {
    type: SET_USER_DATA_ERROR,
    status: FAILURE,
    error,
  };
}
export const updateUserDetail = (userData, userId) => {
  return async (dispatch) => {
    dispatch(setUpdateUserDetailRequesting());
    try{
      const { data } = await axios.post(`${BASE_URL}/api/users/update-user/${userId}`,userData);
      if (data.success) {
        dispatch(setUpdateUserDetailSuccess(data));
      } else {
        dispatch(setUpdateUserDetailError(data.error));  
      }
    } catch(err) {
      if(err.response && err.response.data && err.response.data.error) {
        dispatch(setUpdateUserDetailError(err.response.data.error))
      } else {
        dispatch(setUpdateUserDetailError('Error in handling request.'))
      }
      
    }
  }
}