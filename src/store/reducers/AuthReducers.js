import { 
  SET_LOGIN_ERROR, 
  SET_LOGIN_PENDING, 
  SET_LOGIN_SUCCESS, 
  GET_ERRORS, 
  SET_USER_DETAILS_PENDING, 
  SET_USER_DETAILS_SUCCESS, 
  SET_USER_DETAILS_ERROR 
} from '../types';

const initialState = {
  isLoginPending: false,
  isLoginSuccess: false,
  loginError: null,
  regError:null,
  userName: null,
  isUserDetailsPending: false
}

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_PENDING: 
      return Object.assign({}, state, { isLoginPending: action.isLoginPending })
    case SET_LOGIN_SUCCESS:
      return Object.assign(
        {}, state, 
        { 
          isLoginSuccess: Object.keys(action.userDetails).length ? true : false,
          userName: action.userDetails.name 
        })
    case SET_LOGIN_ERROR:
      return Object.assign({}, state, { loginError: action.loginError })
    case GET_ERRORS:
      return Object.assign({}, state, { regError: action.regError })
    case SET_USER_DETAILS_PENDING:
      return Object.assign({}, state, { isLoginPending: action.isUserDetailsPending })
    case SET_USER_DETAILS_SUCCESS:
      return Object.assign({}, state, { userDetail: action.data })
    case SET_USER_DETAILS_ERROR:
      return Object.assign({}, state, { isLoginPending: action.userDetailsError })
    default:
      return state;
  }
}

export default AuthReducer