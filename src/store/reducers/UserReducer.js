import { 
  SET_USERS_LIST_PENDING, 
  SET_USERS_LIST_SUCCESS, 
  SET_USERS_LIST_ERROR, 
  SET_SINGLE_USER_LIST_PENDING,
  SET_SINGLE_USER_LIST_SUCCESS,
  SET_SINGLE_USER_LIST_ERROR,
  SET_USER_DATA_PENDING,
  SET_USER_DATA_SUCCESS,
  SET_USER_DATA_ERROR
} from '../types';

const initialState = {
  isListOfAllUsersStatus: false,
  listOfAllUsers: {},
  listOfAllUsersError: null,

  singleUserStatus: false,
  singleUser: {},
  singleUserError: null,

  updateUserStatus: false,
  updateUser: {},
  updateUserError: null,
}
const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_USERS_LIST_PENDING: 
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status 
      });
    case SET_USERS_LIST_SUCCESS:
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status,
        listOfAllUsers: action.usersList 
      });
    case SET_USERS_LIST_ERROR:
      return Object.assign({}, state, { 
        isListOfAllUsersStatus: action.status,
        listOfAllUsersError: action.usersListError 
      });

    case SET_SINGLE_USER_LIST_PENDING: 
      return Object.assign({}, state, { 
        singleUserStatus: action.status 
      });
    case SET_SINGLE_USER_LIST_SUCCESS:
      return Object.assign({}, state, { 
        singleUserStatus: action.status,
        singleUser: action.userDetails.data 
      });
    case SET_SINGLE_USER_LIST_ERROR:
      return Object.assign({}, state, { 
        singleUserStatus: action.status,
        singleUserError: action.error 
      });

    case SET_USER_DATA_PENDING: 
      return Object.assign({}, state, { 
        updateUserStatus: action.status 
      });
    case SET_USER_DATA_SUCCESS:
      return Object.assign({}, state, { 
        updateUserStatus: action.status,
        updateUser: action.userDetails.data 
      });
    case SET_USER_DATA_ERROR:
      return Object.assign({}, state, { 
        updateUserStatus: action.status,
        updateUserError: action.error 
      });
    default:
      return state;
  }
}

export default userReducer;