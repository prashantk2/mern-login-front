/* eslint-disable no-undef */
export let BASE_URL = 'https://mern-login-backend.herokuapp.com';
if(process.env.REACT_APP_STATE && process.env.REACT_APP_STATE === 'local') {
  BASE_URL = 'http://localhost:5000';
}

export const REQUESTING = 'requesting';
export const SUCCESS = 'success';
export const FAILURE = 'failure';

export const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING';
export const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';
export const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR';
export const GET_ERRORS = 'GET_ERRORS';
export const SET_USER_DETAILS_PENDING = 'SET_USER_DETAILS_PENDING';
export const SET_USER_DETAILS_ERROR = 'SET_USER_DETAILS_ERROR';
export const SET_USER_DETAILS_SUCCESS = 'SET_USER_DETAILS_SUCCESS';

export const SET_USERS_LIST_PENDING = 'SET_USERS_LIST_PENDING';
export const SET_USERS_LIST_SUCCESS = 'SET_USERS_LIST_SUCCESS';
export const SET_USERS_LIST_ERROR = 'SET_USERS_LIST_ERROR';

export const SET_SINGLE_USER_LIST_PENDING = 'SET_SINGLE_USER_LIST_PENDING';
export const SET_SINGLE_USER_LIST_SUCCESS = 'SET_SINGLE_USER_LIST_SUCCESS';
export const SET_SINGLE_USER_LIST_ERROR = 'SET_SINGLE_USER_LIST_ERROR';

export const SET_USER_DATA_PENDING = 'SET_USER_DATA_PENDING';
export const SET_USER_DATA_SUCCESS = 'SET_USER_DATA_SUCCESS';
export const SET_USER_DATA_ERROR = 'SET_USER_DATA_ERROR';